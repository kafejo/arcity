// Copyright 2007-2014 Metaio GmbH. All rights reserved.

#import <UIKit/UIKit.h>
#include <vector>
#include <MetaioSDK/Common/TrackingValues.h>
#include <metaioSDK/Rendering/IGeometry.h>

#import "LiveViewInterfaceController.h"
#import "MetaioCloudPluginDelegate.h"

@interface MetaioCloudPluginViewController : LiveViewInterfaceController<MetaioCloudPluginDelegate>


/** Removes all content and triggers a new pois search */
- (void) reloadCurrentChannel;


/** Override this method if you want to display your webviewcontroller in a different way */
- (void) presentContentViewController: (UIViewController<MetaioViewControllerClosingCallback>*) contentViewController;


/** Overwrite this method to provide your own viewcontroller for displaying URLs
 * \param url the URL that should be openend
 * \return autoreleased viewcontroller
 *
 * You can use this method to provide your own WebviewController if desired
 */
- (UIViewController<MetaioViewControllerClosingCallback>*) getContentViewControllerForURL:(NSString*) url;


/** This method allows you to (re)set a channel ID that the plugin will use. Calling this method will cause a complete refresh (i.e. remove all objects and load POIs from the given channel ID again.
 *  Note, this Method will not query the delegate
 *
 * \param channelID the channel ID to set
 * \param filters the filter paramters that should be sent to your channel
 */
- (void) openChannelID:(int) channelID withFilterValues:(NSDictionary*) filters;


/** Returns the currenlty loaded channel ID
 * \return the currenlty loaded channel ID
 */
- (int) getCurrentChannelID;


/** Show an info message on the screen
 * \param message the message
 */
- (void) showInfoMessage: (NSString*) message;


/** Overwrite this method if you want to be able to handle custom URL schemes that are launched
 * from your channel
 * \param request the request that is being sent
 *
 * You can use it like this:
 * NSURL* theURL = [request mainDocumentURL];
 * NSString* absoluteString = [theURL absoluteString];
 * if( [[absoluteString lowercaseString] hasPrefix:@"yourapp://"] )
 * {
 *   // do something
 *   return NO;
 * }
 * return YES;
 */
- (BOOL) shouldStartLoadWithRequest:(NSURLRequest *) request;

/** Define the offset of the radar relative to the upper right corner
 * \param offset the offset in x/y
 * \param scale the relative scale of the radar
 * \param the anchor to set. (default: top-right: 10)
 */
- (void) setRadarOffset:(CGPoint) offset scale:(float) scale anchor:(int) anchor;


/** Informs the application when loading has been completed
 * Note: make sure to call [super onSceneReady] when overwriting this method
 */
- (void)  onSceneReady;


/** Callback to inform the client that visiblity has changed
 * \param visiblity new visibility
 */
-(void) onRadarVisibilityChanged: (BOOL) visiblity;


/** Open the sharing controller for a certain image
 * \param imageToShare the image to share.
 */
- (void) openSharingViewController: (UIImage*) imageToShare;



/** Called after storing an image into the gallery */
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;



/**
 * \brief Callback that informs new pose states (tracked, detected or lost)
 *
 * This is called automatically as soon as poses have been updated. The vector
 * contains all the valid poses.
 * The invalid pose is only returned for first frame as soon as target is lost
 * to inform this event.
 * Note that this function is called in rendering thread, thus it would block
 * rendering. It should be returned as soon as possible wihout any expensive
 * processing.
 *
 * \param poses current valid poses
 *
 */
- (void) onTrackingEvent: (const metaio::stlcompat::Vector<metaio::TrackingValues>&)poses;


/** Overwrite this method if you want to provide your own detail view controller
 * \param object arel object that we want to display
 * After closing the poi detail view, call onDetailViewClosed
 * \sa onDetailViewClosed
 */
- (void) openPOIDetail:(const metaio::IARELObject*)object;


/**  If you are handling your own POI detail view, you have to call
 * this method after the poi-detail view is closed
 */
- (void) onDetailViewClosed;

@end
