// Copyright 2007-2014 metaio GmbH. All rights reserved.
//
//  WebViewViewController.h
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "MetaioViewControllerClosingCallback.h"

typedef void (^WebViewViewControllerCallbackBlock)(void);

@interface WebViewViewController : UIViewController<MetaioViewControllerClosingCallback,UIWebViewDelegate, UIPopoverControllerDelegate, WKNavigationDelegate> {

    UIWebView*                          webView;
    WKWebView*                          webViewWk;      // on iOS8 the webkit webview will be used automatically
    UIActivityIndicatorView *           activityIndicator;
}
@property (nonatomic, copy) MetaioActionBlock                       viewDidDisappearAction;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *    activityIndicator;
@property (nonatomic, retain) IBOutlet UIWebView*                   webView;
@property (nonatomic, retain) NSURL*                                url;
@property (nonatomic, retain) IBOutlet UIProgressView*              progressView;   // used from iOS8 webkit webview


- (IBAction) buttonClose;
- (IBAction) buttonSharePushed:(UIBarButtonItem*)sender;
- (IBAction)btnPreviousPushed:(UIBarButtonItem*)sender;
- (IBAction)btnNextPushed:(UIBarButtonItem*)sender;
- (IBAction)btnReloadPushed:(UIBarButtonItem*)sender;



-(id) initializeWithURL: (NSString*) url;

@end
